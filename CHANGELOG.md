# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 0.9.2 - 2019-06-24 - Manual push to repo

- Added manual push to repository for docker images

## 0.9.1 - 2019-05-15 - Change S3 deployment

- Add option `S3_CP_EXTRA_OPTIONS` to allow override metadata

## 0.9.0 - 2019-05-14 - Change S3 deployment and CDN invalidation

- Deploy to CDN using package_name only
- Invalidate CDN on demmand

## 0.8.1 - 2019-05-06 - Deploy docker to registry only on master

- Deploying docker images from MR or branches does not make sense
- Restrict to only master

## 0.8.0 - 2019-04-26 - Configurable coverage regex

- Add possibility to extend test-tests to change coverage

## 0.7.3 - 2019-04-26 - Add changelog-in-mr test

- Add `test/changelog-in-mr.yml` test

## 0.7.1 - 2019-03-15 - Deploy s3 for CDN

- S3 Buckets need a sub-bucket for aws CDN compliance

## 0.7.0 - 2019-03-15 - More generic jobs

- There is now a specific `build-package` job, that make the `dist` folder available through artifact
- The `build-doc` job is now generic, each documentation generation need to be explicitly defined in the package `Makefile`
- Added `deploy-s3` that will upload the `dist` artifact on an S3 bucket
  (the above change require the following env vars `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, `AWS_DEFAULT_REGION`)

## 0.6.0 - 2019-03-15 - Add a build-doc-jsdoc job

- Build jsdoc documentation

## 0.5.0 - 2019-03-15 - Add a deps-install job using make

- In order to be able to use the ci-common file to another env, this allow a job less pythonic

## 0.4.0 - 2019-03-07 - Use new CI_COMMIT_SHORT_SHA and local includes

- Replace `${CI_COMMIT_SHA:0:8}` by ${CI_COMMIT_SHORT_SHA}

## 0.3.11 - 2019-02-21 - Add sonar and update bandit

- Add sonar (in deploy stage)
- Update bandit (use makefile)

## 0.3.10 - 2019-01-24 - Use make sphinx-docs and make pdoc-docs

- Use make for docs

## 0.3.9 - 2018-12-03 - Remove "only" on push package to repository

- Cleaning + fix doc
- Enable push package on pre to repository
- use `set -x` instead of `cat << EOF`

## 0.3.8 - 2018-12-01 - Fix deploy marathon - remove before_script

- before_script in a job override the global before_script => job was broken

## 0.3.7 - 2018-12-01 - Add separated test for pylint

- Add separated test for pylint and another one with `allow_failure`
- It generates a badge for pylint
- Update doc`

## 0.3.6 - 2018-11-30 - Complete the jobs

- Add debug info for upgrade-trigger
- Add PRE in the version if not in master or release branch
- A bit of cleaning
