.PHONY: tests version

tests:
	find . -name "*.yml" -or -name "*.yaml" -exec pipenv run yamllint -d "{extends: default, rules: {line-length: {max: 120}}}" '{}' +

version:
	@grep -E 'Version: (\d+.\d+.\d+)' -R -oh README.md | cut -d':' -f2 | tr -d ' '

deps-install:
	@pipenv sync --dev
	pipenv graph
