# Pipeline jobs

[![version](https://img.shields.io/badge/version-0.9.2-blue.svg)](http://bigdatamedia.gitlab.io/gitlab-ci-common)
[![pipeline status](https://gitlab.com/bigdatamedia/gitlab-ci-common/badges/master/pipeline.svg)](https://gitlab.com/bigdatamedia/gitlab-ci-common/commits/master)

Version: 0.9.2

## Stages

- `env`
- `deps`
- `build`
- `test`
- `deploy`
- `deploy-env`

See below for list of "jobs" per stages

## Files

- `_base`:
  - `_base-test-bandit.yml`: base for tests with bandit
  - `_base-test-pylint.yml`: base for tests with pylint
  - `_base-deploy-registry-docker.yml`: base for deploy the docker to the registry (gitlab)
  - `_base-deploy-registry-package.yml`: base for deploy the package to registry (nexus)
  - `_base-deploy-env-using-ansible.yml`: base for deploy to env using ansible
  - `_base-deploy-env-using-marathon.yml`: base for deploy to env using marathon
  - `_base-deploy-env-using-singularity.yml`: base for deploy to env using singularity
- `env`:
  - `env.yml`: setup the environment (stages, variables, cache) and checks everything is ok (variables present)
- `deps`:
  - `deps-install.yml`: install the deps (pipenv install --dev) and show the graph (pipenv graph)
- `build`:
  - `build-docs.yml`: autogenerate the package's documentation
  - `build-package`: build package and make it available through artifact
  - `build-pages-index.yml`: build the index page from the README.md file
  - `build-spark-job.yml`: build the spark job
  - `build-upgrade-trigger.yml`: trigger an upgrade of a package (given via the variable `UPGRADE_PACKAGE`)
  - `build-version.yml`: build the version.build and the version-badge
- `test`:
  - `test-bandit-allow-failure.yml`: check common security issues (allow failure) (skippable with variable SKIP_TESTS)
  - `test-bandit.yml`: check common security issues (skippable with variable SKIP_TESTS)
  - `test-changelog-in-mr.yml`: check if the mr branch changes the CHANGELOG.md file
  - `test-pylint-allow-failure.yml`: check the coding style with pylint and report the score (allow failure) (skippable with variable SKIP_TESTS) (extends: `.test-pylint`)
  - `test-pylint.yml`: check the coding style with pylint and report the score (skippable with variable SKIP_TESTS) (extends: `.test-pylint`)
  - `test-registry-package-version.yml`: check that the package version is available
  - `test-tests.yml`: execute the tests and report the coverage (skippable with variable SKIP_TESTS)
- `deploy`:
  - `deploy-pages.yml`: deploy the pages (gitlab)
  - `deploy-registry-docker.yml`: deploy the docker to the registry (gitlab) (extends: `.deploy-registry-docker`)
  - `deploy-registry-docker-supervisord.yml`: deploy the docker (supervisord) to the registry (gitlab) (extends: `.deploy-registry-docker`)
  - `deploy-registry-docker-wsgi.yml`: deploy the docker (wsgi) to the registry (gitlab) (extends: `.deploy-registry-docker`)
  - `deploy-registry-package.yml`: deploy the package to registry (nexus) (extends: `.deploy-registry-package`)
  - `deploy-registry-package-pre.yml`: deploy the package to registry (nexus) (extends: `.deploy-registry-package`)
  - `deploy-s3`: deploy the dist folder on a S3 bucket
  - `deploy-sonar.yml`: deploy the different tests report to sonarqube
- `deploy-env`:
  - `deploy-env-dev-using-marathon.yml`: deploy to DEV using marathon (extends: `.deploy-env-using-marathon`)
  - `deploy-env-itt-using-ansible.yml`: deploy to ITT using ansible (extends: `.deploy-env-using-ansible`)
  - `deploy-env-itt-using-marathon.yml`: deploy to ITT using marathon (extends: `.deploy-env-using-marathon`)
  - `deploy-env-uat-using-ansible.yml`: deploy to UAT using ansible (extends: `.deploy-env-using-ansible`)
  - `deploy-env-uat-using-marathon.yml`: deploy to UAT using marathon (extends: `.deploy-env-using-marathon`)
  - `deploy-env-uat-using-singularity.yml`: deploy to UAT using singularity (extends: `.deploy-env-using-singularity`)
  - `deploy-env-prod-using-ansible.yml`: deploy to PROD using ansible (extends: `.deploy-env-using-ansible`)
  - `deploy-env-prod-using-marathon.yml`: deploy to PROD using marathon (extends: `.deploy-env-using-marathon`)
  - `deploy-env-prod-using-singularity.yml`: deploy to PROD using singularity (extends: `.deploy-env-using-singularity`)

*! Important !*
*Until nested includes are supported by Gitlab, when `extends` is specified, you have to include the `_base` file aswell in your `.gitlab-ci.yml` file*

## Variables used

### External variables

_List captured via `grep -E '\$\{[^CI|_|!][a-zA-Z0-9_]{0,}[\}:]' -R -oh --exclude \*.md * | sed 's/.$/\}/' | sort | uniq`_.

`${AWS_ACCESS_KEY_ID}`
`${AWS_SECRET_ACCESS_KEY}`
`${AWS_DEFAULT_REGION}`
`${BUCKET_NAME}`
`${CDN_DISTRIBUTION_ID}`
`${GITLAB_TOKEN}`
`${GITLAB_USER}`
`${INVALIDATE_CDN}`
`${MARATHON_DEV}`
`${MARATHON_ITT}`
`${MARATHON_PROD}`
`${MARATHON_UAT}`
`${S3_CP_EXTRA_OPTIONS}`
`${SINGULARITY_URL_PROD}`
`${SINGULARITY_URL_UAT}`
`${SKIP_TESTS}`
`${TWINE_REPOSITORY_URL}`
`${UPGRADE_PACKAGE}`
`${X_CI_IMAGE}`

### CI Variables

_List captured via `grep -E '\$\{CI_[a-zA-Z0-9_]{0,}[\}:]' -R -oh --exclude \*.md * | sed 's/.$/\}/' | sort | uniq`_.

`${CI_COMMIT_REF_SLUG}`
`${CI_COMMIT_SHORT_SHA}`
`${CI_ENVIRONMENT_NAME}`
`${CI_PROJECT_DIR}`
`${CI_PROJECT_NAME}`
`${CI_REGISTRY}`
`${CI_REGISTRY_IMAGE}`
`${CI_REGISTRY_PASSWORD}`
`${CI_REGISTRY_USER}`

### Internal variables

_List captured via `grep -E '\$\{[_]{1,}[a-zA-Z0-9_]{0,}[\}:]' -R -oh --exclude \*.md * | sed 's/.$/\}/' | sort | uniq`_.

`${_BUCKET_NAME}`
`${_DOCKERFILE}`
`${_EXIT_CODE}`
`${_MARATHON_APP_ID}`
`${_MARATHON_CONN_STRING}`
`${_MARATHON_PASSWORD}`
`${_MARATHON_URL}`
`${_MARATHON_USER}`
`${_PACKAGE_MODULE}`
`${_PACKAGE_NAME}`
`${_PACKAGE_TWINE_PATH}`
`${_PRE}`
`${_PROJECT_NAME_PRETTY}`
`${_PYLINT_SCORE}`
`${_REGISTRY_IMAGE}`
`${_SINGULARITY_URL}`
`${_VERSION}`
`${_VERSION_BUILD}`
`${_VERSION_DATE}`

### Variables exported for existing programs

_List captured via `grep -E 'export [^CI|_|!][a-zA-Z0-9_]{0,}=[^\s]*' -R -oh --exclude \*.md * | sort | uniq`_.

`ANSIBLE_FORCE_COLOR=True`
`PIPENV_VENV_IN_PROJECT=1`
`PIP_CACHE_DIR="${CI_PROJECT_DIR}/.cache/pipenv"`
`PIP_PROCESS_DEPENDENCY_LINKS=1`

## Developer notes

- Do not use `dependencies` since all the artifacts are passed through the different stages
- The cache is centralized (s3) and it must be for the following
- The cache is abused to pass the `.venv` between stages through the cache. The normal way would be with artifacts
